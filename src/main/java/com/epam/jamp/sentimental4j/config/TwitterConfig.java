package com.epam.jamp.sentimental4j.config;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;

import java.util.Properties;

@Configuration
public class TwitterConfig {

    @Bean
    public Twitter streameTwitter(@Value("${spring.social.twitter.appId}") String consumerKey,
                                  @Value("${spring.social.twitter.appSecret}") String consumerSecret,
                                  @Value("${spring.social.twitter.token}") String accessToken,
                                  @Value("${spring.social.twitter.secret}") String accessTokenSecret) {
        return new TwitterTemplate(consumerKey, consumerSecret, accessToken, accessTokenSecret);
    }

    @Bean
    public StanfordCoreNLP stanfordCoreNLP() {
        Properties properties = new Properties();
        properties.put("annotators", "tokenize,ssplit,parse,sentiment");
        return new StanfordCoreNLP(properties);
    }

    @Bean
    public Queue queue(@Value("${rabbit.queue}") String queue) {
        return new Queue(queue);
    }

}
