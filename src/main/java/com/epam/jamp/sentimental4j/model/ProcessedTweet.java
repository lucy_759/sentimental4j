package com.epam.jamp.sentimental4j.model;

import com.epam.jamp.sentimental4j.constants.SentimentalLevel;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ProcessedTweet {
    private String text;
    private SentimentalLevel sentimentalLevel;

    public ProcessedTweet() {
    }

    public ProcessedTweet(String text, SentimentalLevel sentimentalLevel) {
        this.text = text;
        this.sentimentalLevel = sentimentalLevel;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public SentimentalLevel getSentimentalLevel() {
        return sentimentalLevel;
    }

    public void setSentimentalLevel(SentimentalLevel sentimentalLevel) {
        this.sentimentalLevel = sentimentalLevel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("text", text)
                .append("sentimentalLevel", sentimentalLevel)
                .toString();
    }
}
