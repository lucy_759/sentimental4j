package com.epam.jamp.sentimental4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sentimental4jApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sentimental4jApplication.class, args);
    }
}
