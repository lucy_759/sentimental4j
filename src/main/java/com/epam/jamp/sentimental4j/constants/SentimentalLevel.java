package com.epam.jamp.sentimental4j.constants;

import java.util.Arrays;

public enum SentimentalLevel {
    VERY_NEGATIVE(0), NEGATIVE(1), NEUTRAL(2), POSITIVE(3), VERY_POSITIVE(4), UNIDENTIFIED(Integer.MAX_VALUE);

    private int sentimentLevel;

    SentimentalLevel(int sentimentLevel) {
        this.sentimentLevel = sentimentLevel;
    }

    public int getSentimentLevel() {
        return sentimentLevel;
    }

    public static SentimentalLevel valueOfBySentimentLevel(int sentimentLevel) {
        return Arrays.stream(SentimentalLevel.values())
                .filter(sentimentalLevel -> sentimentalLevel.getSentimentLevel() == sentimentLevel)
                .findFirst()
                .orElse(UNIDENTIFIED);
    }
}
