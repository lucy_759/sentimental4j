package com.epam.jamp.sentimental4j.controller;

import com.epam.jamp.sentimental4j.service.TweetStreamingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TwitterConnection {

    @Autowired
    private TweetStreamingService tweetStreamingService;

    @GetMapping("/stream")
    public String streamTweetsInConsoleOnHashtag(String hashtag) {
        tweetStreamingService.stream(hashtag);
        return "Streaming tweets on \'" + hashtag + "\' hashtag in console.";
    }

    @GetMapping("/stop")
    public void stopStreaming() {
        tweetStreamingService.stop();
    }
}

