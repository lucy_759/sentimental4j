package com.epam.jamp.sentimental4j.service;

public interface TweetStreamingService {
    void stream(String hashtag);

    void stop();
}
