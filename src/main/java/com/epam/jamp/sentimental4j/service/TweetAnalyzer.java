package com.epam.jamp.sentimental4j.service;

import com.epam.jamp.sentimental4j.model.ProcessedTweet;
import org.springframework.social.twitter.api.Tweet;

public interface TweetAnalyzer {

    ProcessedTweet analyze(Tweet tweet);

}
