package com.epam.jamp.sentimental4j.service.impl;

import com.epam.jamp.sentimental4j.model.ProcessedTweet;
import com.epam.jamp.sentimental4j.service.TweetProducer;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class TweetProducerImpl implements TweetProducer {

    private final RabbitTemplate template;
    private final Queue queue;

    @Inject
    public TweetProducerImpl(RabbitTemplate template, Queue queue) {
        this.template = template;
        this.queue = queue;
    }

    @Override
    public void send(ProcessedTweet processedTweet) {
        template.convertAndSend(queue.getName(), processedTweet.toString());
    }

}
