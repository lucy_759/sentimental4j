package com.epam.jamp.sentimental4j.service.impl;

import com.epam.jamp.sentimental4j.constants.SentimentalLevel;
import com.epam.jamp.sentimental4j.model.ProcessedTweet;
import com.epam.jamp.sentimental4j.service.TweetAnalyzer;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class TweetAnalyzerImpl implements TweetAnalyzer {

    private final StanfordCoreNLP pipeline;

    @Inject
    public TweetAnalyzerImpl(StanfordCoreNLP pipeline) {
        this.pipeline = pipeline;
    }

    @Override
    public ProcessedTweet analyze(Tweet tweet) {
        int roundedAverageSentimentalLevel = Integer.MAX_VALUE;
        if (StringUtils.isNotBlank(tweet.getText())) {
            Annotation annotation = pipeline.process(tweet.getText());
            double averageSentimentalLevel = annotation.get(CoreAnnotations.SentencesAnnotation.class).stream()
                    .mapToInt(this::findSentimentalLevel)
                    .average()
                    .orElse(Integer.MAX_VALUE);
            roundedAverageSentimentalLevel = (int) Math.round(averageSentimentalLevel);
        }
        return new ProcessedTweet(tweet.getText(), SentimentalLevel.valueOfBySentimentLevel(roundedAverageSentimentalLevel));
    }

    private int findSentimentalLevel(CoreMap sentence) {
        Tree tree = sentence.get(SentimentCoreAnnotations.SentimentAnnotatedTree.class);
        return RNNCoreAnnotations.getPredictedClass(tree);
    }

}
