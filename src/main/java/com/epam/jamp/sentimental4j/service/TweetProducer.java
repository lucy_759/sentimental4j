package com.epam.jamp.sentimental4j.service;

import com.epam.jamp.sentimental4j.model.ProcessedTweet;

public interface TweetProducer {

    void send(ProcessedTweet processedTweet);

}
