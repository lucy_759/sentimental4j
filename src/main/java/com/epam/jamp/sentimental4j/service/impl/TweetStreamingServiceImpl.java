package com.epam.jamp.sentimental4j.service.impl;

import com.epam.jamp.sentimental4j.service.TweetAnalyzer;
import com.epam.jamp.sentimental4j.service.TweetStreamingService;
import org.springframework.social.twitter.api.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.epam.jamp.sentimental4j.constants.AppConstants.ENGLISH_LANGUAGE_CODE;

@Service
public class TweetStreamingServiceImpl implements TweetStreamingService {

    private final Twitter twitter;
    private final TweetAnalyzer tweetAnalyzer;
    private final TweetProducerImpl producer;

    private List<StreamListener> listeners;
    private Stream stream;

    @Inject
    public TweetStreamingServiceImpl(Twitter twitter, TweetAnalyzer tweetAnalyzer, TweetProducerImpl producer) {
        this.twitter = twitter;
        this.tweetAnalyzer = tweetAnalyzer;
        this.producer = producer;
    }

    @Override
    public void stream(String hashtag) {
        stream = twitter.streamingOperations().filter(hashtag, listeners);
    }

    @Override
    public void stop() {
        closeStream();
    }

    @PostConstruct
    public void init() {
        StreamListener hashtagListener = new StreamListener() {
            @Override
            public void onTweet(Tweet tweet) {
                if (ENGLISH_LANGUAGE_CODE.equals(tweet.getLanguageCode())) {
                    producer.send((tweetAnalyzer.analyze(tweet)));
                    System.out.println(tweetAnalyzer.analyze(tweet));
                }
            }

            @Override
            public void onDelete(StreamDeleteEvent deleteEvent) {
            }

            @Override
            public void onLimit(int numberOfLimitedTweets) {
            }

            @Override
            public void onWarning(StreamWarningEvent warningEvent) {
            }
        };
        listeners = Collections.singletonList(hashtagListener);
    }

    @PreDestroy
    public void closeStream() {
        if (Objects.nonNull(stream)) {
            stream.close();
        }
    }
}
